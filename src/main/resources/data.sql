INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (1, 'Ostatnie życzenie. Wiedźmin', 'Sapkowski Andrzej', 29.99, '9788375781250', 'fantasy', 330, 'Supernowa', '2014-10-06', 15830982, 'polski', 'https://image.ceneostatic.pl/data/products/32692858/i-wiedzmin-1-ostatnie-zyczenie.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (2, 'Kotlin w Akcji', 'Dmitry Jemerov', 39.99, '9788328347205', 'podręczniki akademickie', 352, 'Helion', '2018-11-26', 27063675, 'polski', 'https://static01.helion.com.pl/global/okladki/326x466/kotakc.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (3, 'Harry Potter i Kamień Filozoficzny', 'Rowling J.K.', 24.99, '9780439362139', 'fantasy' , 328, 'Media Rodzina', '2016-07-20', 19661162, 'polski', 'https://image.ceneostatic.pl/data/products/18283/i-harry-potter-i-kamien-filozoficzny.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (4, 'Hobbit, czyli tam i z powrotem', 'Tolkien John Ronald Reuel', 45.99, '9780044403371', 'fantasy' , 280, 'Wydawnictwo Iskry', '2012-09-11', 11768791, 'polski', 'https://image.ceneostatic.pl/data/products/32617009/i-hobbit-czyli-tam-i-z-powrotem-r-2007.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (5, 'Kuba', 'Błaszczykowski Jakub, Domagalik Małgorzata', 19.99, '9788328055698', 'biografia', 232, 'Wydawnictwo Buchmann', '2018-05-23', 26014593, 'polski', 'https://cdn-lubimyczytac.pl/upload/books/251000/251400/371717-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (6, 'Billy Summers', 'Stephen King', 69.99, '1248375781250', 'kryminał, sensacja, thriller', 730, 'Prószyński i S-ka', '2014-12-16', 12430982, 'polski', 'https://s.lubimyczytac.pl/upload/books/4973000/4973985/911099-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (7, '27 śmierci Toby’ego Obeda', 'Joanna Gierak-Onoszko', 45.99, '9788328347205', 'reportaż', 652, 'Helion', '2003-01-06', 27655675, 'polski', 'https://s.lubimyczytac.pl/upload/books/4882000/4882986/866945-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (8, 'Wybaczam ci', 'Remigiusz Mróz', 34.99, '1286745862139', 'kryminał, sensacja, thriller' , 528, 'Czwarta Strona', '2013-02-01', 1781162, 'polski', 'https://s.lubimyczytac.pl/upload/books/4975000/4975543/913781-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (9, 'Bez pożegnania', 'Harlan Coben', 15.99, '120045603371', 'kryminał, sensacja, thriller' , 640, 'Albatros', '2017-09-21', 11765591, 'polski', 'https://s.lubimyczytac.pl/upload/books/4974000/4974990/912779-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (10, 'Diuna', 'Frank Herbert', 89.99, '3324328055291', 'fantasy, science fiction', 252, 'Rebis', '2008-11-03', 12014593, 'polski', 'https://s.lubimyczytac.pl/upload/books/37000/37752/580265-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (11, 'Pieśń o Achillesie', 'Madeline Miller', 59.99, '2288371811240', 'fantasy, science fiction', 980, 'Albatros', '2000-10-16', 95856982, 'polski', 'https://s.lubimyczytac.pl/upload/books/4965000/4965293/903258-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (12, 'Matnia', 'Przemysław Piotrowski', 35.99, '9788328347205', 'kryminał, sensacja, thriller', 732, 'Czarna Owca', '1999-11-06', 27893675, 'polski', 'https://s.lubimyczytac.pl/upload/books/4976000/4976452/917553-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (13, 'Ostatni dom na zapomnianej ulicy', 'Catriona Ward', 14.99, '6870439324139', 'kryminał, sensacja, thriller' , 148, 'Czwarta Strona', '2010-07-25', 12361162, 'polski', 'https://s.lubimyczytac.pl/upload/books/4974000/4974011/916294-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (14, 'Czuła przewodniczka. Kobieca droga do siebie', 'Natalia de Barbaro', 55.99, '6488004440371', 'nauki społeczne' , 652, 'Agora', '2002-12-12', 66768791, 'polski', 'https://s.lubimyczytac.pl/upload/books/4955000/4955733/875320-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (15, 'Nagie serca', 'Colleen Hoover', 99.99, '446328058998', 'literatura obyczajowa, romans', 752, 'Otwarte', '2008-02-13', 26014593, 'polski', 'https://ecsmedia.pl/c/nagie-serca-b-iext72775212.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (16, 'Siedem sióstr', 'Lucinda Riley', 95.99, '3488324581250', 'literatura obyczajowa, romans', 320, 'Albatros', '2004-10-16', 77730982, 'polski', 'https://s.lubimyczytac.pl/upload/books/4899000/4899052/757670-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (17, 'Narzeczona nazisty', 'Barbara Wysoczańska', 139.99, '9788328347205', 'literatura obyczajowa, romans', 672, 'Filia', '2011-01-06', 26663375, 'polski', 'https://s.lubimyczytac.pl/upload/books/4973000/4973499/912434-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (18, 'Wojna makowa', 'Rebecca F. Kuang', 124.99, '952439346133', 'fantasy, science fiction' , 828, 'Fabryka Słów', '2012-02-21', 79562161, 'polski', 'https://s.lubimyczytac.pl/upload/books/4909000/4909870/783951-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (19, 'Szóstka wron', 'Leigh Bardugo', 145.99, '648244402311', 'fantasy, science fiction' , 1280, 'Mag', '2014-05-12', 15662791, 'polski', 'https://s.lubimyczytac.pl/upload/books/309000/309434/534488-352x500.jpg', 1);
INSERT INTO BOOK(id, title, author, price, ISBN, genre, page_size, publishing_house, release_date, index, original_language, image_url, count_size) VALUES
                (20, 'Devil', 'Julia Brylewska', 119.99, '765828055295', 'literatura obyczajowa, romans', 622, 'NieZwykłe', '2017-04-22', 26458693, 'polski', 'https://s.lubimyczytac.pl/upload/books/4971000/4971335/914045-352x500.jpg', 1);

INSERT INTO my_user(id, username, password, email, role, user_code) VALUES (1, 'Despo', '123', 'despo@dd.com', 'ADMIN', '123');
INSERT INTO my_user(id, username, password, email, role, user_code) VALUES (2, 'User', '123', 'user@dd.com', 'USER', '456');
