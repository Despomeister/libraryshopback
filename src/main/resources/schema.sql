CREATE TABLE my_user (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR2(255) NOT NULL,
    password VARCHAR2(255) NOT NULL,
    email VARCHAR2(255) NOT NULL,
    role VARCHAR2(255) NOT NULL,
    user_code VARCHAR2(255) NOT NULL
);

CREATE TABLE book (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR2(255) NOT NULL,
    author VARCHAR2(255) NOT NULL,
    price DOUBLE NOT NULL,
    ISBN VARCHAR2(255),
    genre VARCHAR2(255) NOT NULL,
    page_size INTEGER,
    publishing_house VARCHAR2(255),
    release_date VARCHAR2(255),
    index INTEGER,
    original_language VARCHAR2(255),
    image_url VARCHAR2(255),
    count_size BIGINT
);

CREATE TABLE cart
(
    id BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    id_user BIGINT NOT NULL,
    id_book BIGINT
);
