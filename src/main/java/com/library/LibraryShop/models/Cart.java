package com.library.LibraryShop.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;

@AllArgsConstructor
@Entity
@Getter
public class Cart {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(nullable = false)
    private Long id_user;

    private Long id_book;

    public Cart() {
    }
}
