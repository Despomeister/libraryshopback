package com.library.LibraryShop.models;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@EqualsAndHashCode
@AllArgsConstructor
@Entity
@Getter
@Setter
public class Book {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private double price;

    private String ISBN;

    private String genre;

    @Column(name = "page_size")
    private Long pageSize;

    @Column(name = "publishing_house")
    private String publishingHouse;

    @Column(name = "release_date")
    private String releaseDate;

    private Long index;

    @Column(name = "original_language")
    private String originalLanguage;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "count_size")
    private Long countSize;

    public Book() {
    }
}
