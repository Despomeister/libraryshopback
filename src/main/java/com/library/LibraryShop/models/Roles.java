package com.library.LibraryShop.models;

public enum Roles {
    ADMIN, USER, VENDOR, TECHNICAL_SERVICE
}
