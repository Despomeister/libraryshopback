package com.library.LibraryShop.repository;
import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.List;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query(value = "select b from Book b where b.id in (select id_book from Cart where id_user = :id_user)")
    List<Book> booksInCart(@Param("id_user") Long id_user);

    @Transactional
    @Modifying
    @Query(value = "delete from Cart b where b.id_book = :id_book and b.id_user = :id_user")
    void deleteBookFromCartById(@Param("id_user") Long userId, @Param("id_book") Long bookId);
}
