package com.library.LibraryShop.repository;

import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Optional;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface BookRepository extends JpaRepository<Book, Long> {
    Optional<Book> findBookById(Long id);

    void deleteBookById(Long id);
}
