package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.User;
import com.library.LibraryShop.service.RegistrationService;
import com.library.LibraryShop.service.UserService;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

    private final RegistrationService registrationService;

    private final UserService userService;

    RegistrationController(RegistrationService registrationService, UserService userService) {
        this.registrationService = registrationService;
        this.userService = userService;
    }

    @PostMapping("/registerUser")
    @CrossOrigin(origins = "http://localhost:4200")
    public User registerUser(@RequestBody User user) {
        String userEmail = user.getEmail();
        if (!ObjectUtils.isEmpty(userEmail)) {
            User tempUser = registrationService.findUserByEmail(userEmail);
            if (tempUser != null) {
                throw new RuntimeException("User with " + userEmail + " is already register.");
            }
        }
        return userService.addUser(user);
    }

    @PostMapping("/loginUser")
    @CrossOrigin(origins = "http://localhost:4200")
    public User loginUser(@RequestBody User user) {
        String userEmail = user.getEmail();
        String userPassword = user.getPassword();
        User userToLogin = registrationService.findUserByEmailAndPassword(userEmail, userPassword);
        if (userToLogin == null) {
            throw new RuntimeException("User with " + userEmail + " isn't exist or password is wrong!");
        }
        return userToLogin;
    }
}
