package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/all")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Book> getAllBooks () {
        return bookService.getBooks();
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public Book getBookById (@PathVariable("id") Long id) {
        return bookService.findBookById(id);
    }

    @PostMapping("/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public Book addBook(@RequestBody Book book) {
        return bookService.addBook(book);
    }

    @PutMapping("/update")
    @CrossOrigin(origins = "http://localhost:4200")
    public Book updateBook(@RequestBody Book book) {
        return bookService.updateBook(book);
    }

    @DeleteMapping("/delete/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public void deleteBook(@PathVariable("id") Long id) {
        bookService.deleteBook(id);
    }
}
