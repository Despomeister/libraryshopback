package com.library.LibraryShop.controllers;
import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.models.Cart;
import com.library.LibraryShop.service.CartService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping("/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public Cart addToCart(@RequestBody Cart cart) {
        return cartService.addToCart(cart);
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Book> getBooksInCartById (@PathVariable("id") Long id) {
        return cartService.getBooksFromCart(id);
    }

    @DeleteMapping("/delete/{userId}/{bookId}")
    @CrossOrigin(origins = "http://localhost:4200")
    public void deleteBookFromCart(@PathVariable("userId") Long userId, @PathVariable("bookId") Long bookId) {
        cartService.deleteBookFromCart(userId, bookId);
    }
}
