package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.User;
import com.library.LibraryShop.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<User> getAllUsers () {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public User getUserById (@PathVariable("id") Long id) {
        return userService.findUserById(id);
    }

    @PostMapping("/add")
    @CrossOrigin(origins = "http://localhost:4200")
    public User addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @PutMapping("/update")
    @CrossOrigin(origins = "http://localhost:4200")
    public User updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @DeleteMapping("/delete/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }
}
