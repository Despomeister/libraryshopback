package com.library.LibraryShop.service;

import com.library.LibraryShop.exceptions.UserNotFoundException;
import com.library.LibraryShop.models.User;
import com.library.LibraryShop.repository.RegistrationRepository;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private final RegistrationRepository registrationRepository;

    public RegistrationService(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    public User findUserByEmail(String email) {
        return registrationRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException("User by email " + email + " was not found"));
    }

    public User findUserByEmailAndPassword(String email, String password) {
        return registrationRepository.findByEmailAndPassword(email, password).orElseThrow(() -> new UserNotFoundException("User by email " + email + " was not found"));
    }

}
