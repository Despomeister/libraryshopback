package com.library.LibraryShop.service;

import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.models.Cart;
import com.library.LibraryShop.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CartService {

    private final CartRepository cartRepository;

    @Autowired
    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public Cart addToCart(Cart cart) {
        return this.cartRepository.save(cart);
    }

    public List<Book> getBooksFromCart(Long id_user) {
        return this.cartRepository.booksInCart(id_user);
    }

    @Transactional
    public void deleteBookFromCart(Long userId, Long bookId) {
        cartRepository.deleteBookFromCartById(userId, bookId);
    }
}
