package com.library.LibraryShop.service;

import com.library.LibraryShop.exceptions.UserNotFoundException;
import com.library.LibraryShop.models.User;
import com.library.LibraryShop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(User user) {
        user.setUserCode(UUID.randomUUID().toString());
        return userRepository.save(user);
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Transactional
    public User updateUser(User user) {
        User updatedUser = userRepository.findUserById(user.getId()).orElseThrow(() -> new UserNotFoundException("User by id " + user.getId() + " was not found"));
        updatedUser.setUserCode(user.getUserCode());
        updatedUser.setUsername(user.getUsername());
        updatedUser.setEmail(user.getEmail());
        updatedUser.setPassword(user.getPassword());
        updatedUser.setRole(user.getRole());
        return updatedUser;
    }

    public User findUserById(Long id) {
        return userRepository.findUserById(id).orElseThrow(() -> new UserNotFoundException("User by id " + id + " was not found"));
    }

    @Transactional
    public void deleteUser(Long id) {
        userRepository.deleteUserById(id);
    }
}
