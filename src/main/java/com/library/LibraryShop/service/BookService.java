package com.library.LibraryShop.service;

import com.library.LibraryShop.exceptions.BookNotFoundException;
import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book addBook(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    @Transactional
    public Book updateBook(Book book) {
        Book updatedBook = bookRepository.findBookById(book.getId()).orElseThrow(() -> new BookNotFoundException("Book by id " + book.getId() + " was not found"));
        updatedBook.setTitle(book.getTitle());
        updatedBook.setAuthor(book.getAuthor());
        updatedBook.setGenre(book.getGenre());
        updatedBook.setImageUrl(book.getImageUrl());
        updatedBook.setISBN(book.getISBN());
        updatedBook.setOriginalLanguage(book.getOriginalLanguage());
        updatedBook.setPublishingHouse(book.getPublishingHouse());
        updatedBook.setIndex(book.getIndex());
        updatedBook.setPageSize(book.getPageSize());
        updatedBook.setReleaseDate(book.getReleaseDate());
        updatedBook.setPrice(book.getPrice());
        return updatedBook;
    }

    public Book findBookById(Long id) {
        return bookRepository.findBookById(id).orElseThrow(() -> new BookNotFoundException("Book by id " + id + " was not found"));
    }

    @Transactional
    public void deleteBook(Long id) {
        bookRepository.deleteBookById(id);
    }
}
