package com.library.LibraryShop.service;

import com.library.LibraryShop.exceptions.BookNotFoundException;
import com.library.LibraryShop.exceptions.UserNotFoundException;
import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.models.Roles;
import com.library.LibraryShop.models.User;
import com.library.LibraryShop.repository.BookRepository;
import com.library.LibraryShop.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UserServiceTest {

    @Test
    void addUser() {
        //Given
        UserRepository userRepository = mock(UserRepository.class);
        User user = exampleGetUsersMockDate().get(0);
        given(userRepository.save(user)).willReturn(user);
        UserService userService = new UserService(userRepository);
        //When
        User userToAdd = userService.addUser(user);
        //Then
        assertEquals(userToAdd, user);
    }

    @Test
    void getUsers() {
        //Given
        UserRepository userRepository = mock(UserRepository.class);
        given(userRepository.findAll()).willReturn(exampleGetUsersMockDate());
        UserService userService = new UserService(userRepository);
        //When
        List<User> users = userService.getUsers();
        //Then
        assertEquals(users, exampleGetUsersMockDate());
    }

    @Test
    void updateUser() {
        //Given
        UserRepository userRepository = mock(UserRepository.class);
        User user = exampleGetUsersMockDate().get(0);
        User badUser = exampleGetUsersMockDate().get(1);
        given(userRepository.findUserById(user.getId())).willReturn(Optional.of(user));
        UserService userService = new UserService(userRepository);
        //When
        User userTest = userService.updateUser(user);
        //Then
        assertEquals(userTest, user);
        assertThrows(UserNotFoundException.class, () -> userService.updateUser(badUser));
    }

    @Test
    void findUserById() {
        //Given
        UserRepository userRepository = mock(UserRepository.class);
        Long userId = 1L;
        Long badUserId = 2L;
        Optional<User> tempUser = exampleGetUserByIdMockDate(userId);
        given(userRepository.findUserById(userId)).willReturn(tempUser);
        UserService userService = new UserService(userRepository);
        //When
        User user = userService.findUserById(userId);
        //Then
        assertEquals(Optional.of(user), tempUser);
        assertThrows(UserNotFoundException.class, () -> userService.findUserById(badUserId));
    }

    @Test
    void deleteUser() {
        //Given
        UserRepository userRepository = mock(UserRepository.class);
        UserService userService = new UserService(userRepository);
        long userId = 1L;
        //When
        userService.deleteUser(userId);
        //Then
        verify(userRepository, times(1)).deleteUserById(userId);
    }

    private List<User> exampleGetUsersMockDate() {
        List<User> users = new ArrayList<>();
        users.add(new User(
                1L,
                "User_1",
                "Email_1",
                "Code_1",
                Roles.USER,
                "Haslo_1"
        ));
        users.add(new User(
                2L,
                "User_2",
                "Email_2",
                "Code_2",
                Roles.USER,
                "Haslo_2"

        ));
        return users;
    }

    private Optional<User> exampleGetUserByIdMockDate(long userId) {
        var ref = new User() {
            User user = new User();
        };
        List<User> users = exampleGetUsersMockDate();
        users.forEach(u -> {
            if (u.getId() == userId)
            {
                ref.user = u;
            }
        });
        return Optional.ofNullable(ref.user);
    }

    private Optional<User> exampleGetUserByUsernameMockDate(String userUsername) {
        var ref = new User() {
            User user = new User();
        };
        List<User> users = exampleGetUsersMockDate();
        users.forEach(u -> {
            if (u.getUsername().equals(userUsername))
            {
                ref.user = u;
            }
        });
        return Optional.ofNullable(ref.user);
    }
}