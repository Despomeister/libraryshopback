package com.library.LibraryShop.service;

import com.library.LibraryShop.controllers.RegistrationController;
import com.library.LibraryShop.models.Roles;
import com.library.LibraryShop.models.User;
import com.library.LibraryShop.repository.RegistrationRepository;
import com.library.LibraryShop.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class RegistrationServiceTest {

    @Test
    void fetchUserByEmail() {
        //Given
        RegistrationRepository registrationRepository = mock(RegistrationRepository.class);
        User tempUser = exampleGetUsersMockDate().get(0);
        User badUser = exampleGetUsersMockDate().get(1);
        given(registrationRepository.findByEmail(tempUser.getEmail())).willReturn(Optional.of(tempUser));
        RegistrationService registrationService = new RegistrationService(registrationRepository);
        //When
        User user = registrationService.findUserByEmail(tempUser.getEmail());
        //Then
        assertEquals(user, tempUser);
        Assertions.assertThrows(RuntimeException.class, () -> registrationService.findUserByEmail(badUser.getEmail()));
    }

    @Test
    void fetchUserByEmailAndPassword() {
        //Given
        RegistrationRepository registrationRepository = mock(RegistrationRepository.class);
        User tempUser = exampleGetUsersMockDate().get(0);
        User badUser = exampleGetUsersMockDate().get(1);
        given(registrationRepository.findByEmailAndPassword(tempUser.getEmail(), tempUser.getPassword()))
                .willReturn(Optional.of(tempUser));
        RegistrationService registrationService = new RegistrationService(registrationRepository);
        //When
        User user = registrationService.findUserByEmailAndPassword(tempUser.getEmail(), tempUser.getPassword());
        //Then
        assertEquals(tempUser, user);
        Assertions.assertThrows(RuntimeException.class, () -> registrationService.findUserByEmailAndPassword(
                badUser.getEmail(), badUser.getPassword()));
    }

    private List<User> exampleGetUsersMockDate() {
        List<User> users = new ArrayList<>();
        users.add(new User(
                1L,
                "User_1",
                "Haslo_1",
                "Email_1",
                Roles.USER,
                "Code_1"
        ));
        users.add(new User(
                2L,
                "User_2",
                "Haslo_2",
                "Email_2",
                Roles.USER,
                "Code_2"

        ));
        return users;
    }
}