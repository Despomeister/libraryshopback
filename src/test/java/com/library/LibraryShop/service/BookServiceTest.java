package com.library.LibraryShop.service;

import com.library.LibraryShop.exceptions.BookNotFoundException;
import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.repository.BookRepository;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class BookServiceTest {

    @Test
    void addBook() {
        //Given
        BookRepository bookRepository = mock(BookRepository.class);
        Book book = exampleGetBooksMockDate().get(0);
        given(bookRepository.save(book)).willReturn(book);
        BookService bookService = new BookService(bookRepository);
        //When
        Book bookToAdd = bookService.addBook(book);
        //Then
        assertEquals(bookToAdd, book);
    }

    @Test
    void getBooks() {
        //Given
        BookRepository bookRepository = mock(BookRepository.class);
        given(bookRepository.findAll()).willReturn(exampleGetBooksMockDate());
        BookService bookService = new BookService(bookRepository);
        //When
        List<Book> books = bookService.getBooks();
        //Then
        assertEquals(books, exampleGetBooksMockDate());
    }

    @Test
    void updateBook() {
        //Given
        BookRepository bookRepository = mock(BookRepository.class);
        Book book = exampleGetBooksMockDate().get(0);
        Book badBook = exampleGetBooksMockDate().get(1);
        given(bookRepository.findBookById(book.getId())).willReturn(Optional.of(book));
        BookService bookService = new BookService(bookRepository);
        //When
        Book bookTest = bookService.updateBook(book);
        //Then
        assertEquals(bookTest, book);
        assertThrows(BookNotFoundException.class, () -> bookService.updateBook(badBook));
    }

    @Test
    void findBookById() {
        //Given
        BookRepository bookRepository = mock(BookRepository.class);
        given(bookRepository.findBookById(1L)).willReturn(exampleGetBookByIdMockDate(1L));
        BookService bookService = new BookService(bookRepository);
        //When
        Book book = bookService.findBookById(1L);
        //Then
        assertEquals(Optional.of(book), exampleGetBookByIdMockDate(1L));
        assertThrows(BookNotFoundException.class, () -> bookService.findBookById(2L));
    }

    @Test
    void deleteBook() {
        //Given
        BookRepository bookRepository = mock(BookRepository.class);
        BookService bookService = new BookService(bookRepository);
        long bookId = 1L;
        //When
        bookService.deleteBook(bookId);
        //Then
        verify(bookRepository, times(1)).deleteBookById(bookId);
    }

    private List<Book> exampleGetBooksMockDate() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1L,
                "Tytul_1",
                "Autor_1",
                25.50,
                "ISBN_1",
                "Gatunek_1",
                111L,
                "Wydawnictwo_1",
                "Data_wydania_1",
                111L,
                "Jezyk_1",
                "Link_Zdjecia_1",
                1L)
        );
        books.add(new Book(
                2L,
                "Tytul_2",
                "Autor_2",
                35.50,
                "ISBN_2",
                "Gatunek_2",
                222L,
                "Wydawnictwo_2",
                "Data_wydania_2",
                222L,
                "Jezyk_2",
                "Link_Zdjecia_2",
                1L)
        );
        return books;
    }

    private Optional<Book> exampleGetBookByIdMockDate(long bookId) {
        var ref = new Book() {
            Book book = new Book();
        };
        List<Book> books = exampleGetBooksMockDate();
        books.forEach(b -> {
            if (b.getId() == bookId)
            {
                ref.book = b;
            }
        });
        return Optional.ofNullable(ref.book);
    }
}