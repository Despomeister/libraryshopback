package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.service.BookService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class BookControllerTest {

    @Test
    void getAllBooks() {
        //Given
        BookService bookService = mock(BookService.class);
        given(bookService.getBooks()).willReturn(exampleGetBooksMockDate());
        BookController bookController = new BookController(bookService);
        //When
        List<Book> books = bookController.getAllBooks();
        //Then
        assertEquals(books, exampleGetBooksMockDate());
    }

    @Test
    void getBookById() {
        //Given
        BookService bookService = mock(BookService.class);
        given(bookService.findBookById(1L)).willReturn(exampleGetBookByIdMockDate(1L));
        BookController bookController = new BookController(bookService);
        //When
        Book book = bookController.getBookById(1L);
        //Then
        assertEquals(book, exampleGetBookByIdMockDate(1L));
    }

    @Test
    void addBook() {
        //Given
        BookService bookService = mock(BookService.class);
        Book tempBook = exampleGetBookByIdMockDate(3L);
        given(bookService.addBook(tempBook)).willReturn(tempBook);
        BookController bookController = new BookController(bookService);
        //When
        Book book = bookController.addBook(tempBook);
        //Then
        assertEquals(book, exampleGetBookByIdMockDate(3L));
    }

    @Test
    void updateBook() {
        //Given
        BookService bookService = mock(BookService.class);
        Book tempBook = exampleGetBookByIdMockDate(1L);
        given(bookService.updateBook(tempBook)).willReturn(tempBook);
        BookController bookController = new BookController(bookService);
        //When
        Book book = bookController.updateBook(tempBook);
        //Then
        assertEquals(book, tempBook);
    }

    @Test
    void deleteBook() {
        //Given
        BookService bookService = mock(BookService.class);
        BookController bookController = new BookController(bookService);
        long bookId = 1L;
        //When
        bookController.deleteBook(bookId);
        //Then
        verify(bookService, times(1)).deleteBook(bookId);
    }

    private List<Book> exampleGetBooksMockDate() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1L,
                "Tytul_1",
                "Autor_1",
                25.50,
                "ISBN_1",
                "Gatunek_1",
                111L,
                "Wydawnictwo_1",
                "Data_wydania_1",
                111L,
                "Jezyk_1",
                "Link_Zdjecia_1",
                1L)
        );
        books.add(new Book(
                2L,
                "Tytul_2",
                "Autor_2",
                35.50,
                "ISBN_2",
                "Gatunek_2",
                222L,
                "Wydawnictwo_2",
                "Data_wydania_2",
                222L,
                "Jezyk_2",
                "Link_Zdjecia_2",
                1L)
        );
        books.add(new Book (
                3L,
                "Tytul_3",
                "Autor_3",
                35.50,
                "ISBN_3",
                "Gatunek_3",
                333L,
                "Wydawnictwo_3",
                "Data_wydania_3",
                333L,
                "Jezyk_3",
                "Link_Zdjecia_3",
                3L)
        );
        return books;
    }

    private Book exampleGetBookByIdMockDate(long bookId) {
        var ref = new Book() {
            Book book = new Book();
        };
        List<Book> books = exampleGetBooksMockDate();
        books.forEach(b -> {
            if (b.getId() == bookId)
            {
                ref.book = b;
            }
        });
        return ref.book;
//        return exampleGetBooksMockDate().stream().filter(book -> Objects.equals(book.getId(), bookId)).findFirst();
    }
}