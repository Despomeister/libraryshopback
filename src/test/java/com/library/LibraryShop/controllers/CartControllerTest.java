package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.Book;
import com.library.LibraryShop.models.Cart;
import com.library.LibraryShop.service.CartService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class CartControllerTest {

    @Test
    void addToCart() {
        //Given
        CartService cartService = mock(CartService.class);
        Cart tempCart = exampleGetCartsMockDate().get(0);
        given(cartService.addToCart(tempCart)).willReturn(tempCart);
        CartController cartController = new CartController(cartService);
        //When
        Cart cart = cartController.addToCart(tempCart);
        //Then
        assertEquals(cart, tempCart);
    }

    @Test
    void getBooksInCartById() {
        //Given
        CartService cartService = mock(CartService.class);
        List<Cart> carts = exampleGetCartsMockDate();
        Cart cart = exampleGetCartsMockDate().get(0);
        List<Book> books = exampleGetBooksFromCartByIdMockDate(carts,cart);
        given(cartService.getBooksFromCart(cart.getId_user())).willReturn(books);
        CartController cartController = new CartController(cartService);
        //When
        List<Book> booksFromCart = cartController.getBooksInCartById(cart.getId_user());
        //Then
        assertEquals(books, booksFromCart);
    }

    @Test
    void deleteBookFromCart() {
        //Given
        CartService cartService = mock(CartService.class);
        CartController cartController = new CartController(cartService);
        List<Cart> carts = exampleGetCartsMockDate();
        Cart cart = carts.get(0);
        long bookId = cart.getId_book();
        //When
        cartController.deleteBookFromCart(cart.getId_user(), bookId);
        //Then
        verify(cartService, times(1)).deleteBookFromCart(cart.getId_user(), bookId);
    }

    public List<Book> exampleGetBooksMockDate() {
        List<Book> books = new ArrayList<>();
        books.add(new Book(
                1L,
                "Tytul_1",
                "Autor_1",
                25.50,
                "ISBN_1",
                "Gatunek_1",
                111L,
                "Wydawnictwo_1",
                "Data_wydania_1",
                111L,
                "Jezyk_1",
                "Link_Zdjecia_1",
                1L)
        );
        books.add(new Book(
                2L,
                "Tytul_2",
                "Autor_2",
                35.50,
                "ISBN_2",
                "Gatunek_2",
                222L,
                "Wydawnictwo_2",
                "Data_wydania_2",
                222L,
                "Jezyk_2",
                "Link_Zdjecia_2",
                1L)
        );
        return books;
    }

    private List<Cart> exampleGetCartsMockDate() {
        List<Book> books = exampleGetBooksMockDate();
        List<Cart> carts = new ArrayList<>();
        carts.add(new Cart(1L,1L,books.get(0).getId()));
        carts.add(new Cart(2L,1L,books.get(1).getId()));
        return carts;
    }

    private List<Book> exampleGetBooksFromCartByIdMockDate(List<Cart> carts, Cart cart) {

        List<Long> booksId = new ArrayList<>();
        carts.forEach(c -> {
            if (c.getId_user().equals(cart.getId_user())) {
                booksId.add(c.getId_book());
            }
        });

        List<Book> books = exampleGetBooksMockDate();
        var ref = new Object() {
            final List<Book> booksFromCart = new ArrayList<>();
        };
        books.forEach(b -> {
            booksId.forEach(i -> {
                if (b.getId().equals(i)) {
                    ref.booksFromCart.add(b);
                }
            });
        });
        return ref.booksFromCart;
    }
}