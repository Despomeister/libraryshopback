package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.Roles;
import com.library.LibraryShop.models.User;
import com.library.LibraryShop.service.RegistrationService;
import com.library.LibraryShop.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class RegistrationControllerTest {

    @Test
    void registerUser() {
        //Given
        RegistrationService registrationService = mock(RegistrationService.class);
        User tempUser = exampleGetUsersMockDate().get(0);
        User badUser = exampleGetUsersMockDate().get(1);
        UserService userService = mock(UserService.class);
        given(userService.addUser(tempUser)).willReturn(tempUser);
        given(registrationService.findUserByEmail(badUser.getEmail())).willReturn(badUser);
        RegistrationController registrationController = new RegistrationController(registrationService, userService);
        //When
        User user = registrationController.registerUser(tempUser);
        //Then
        assertEquals(user, tempUser);
        Assertions.assertThrows(RuntimeException.class, () -> registrationController.registerUser(badUser));
    }

    @Test
    void loginUser()  {
        //Given
        RegistrationService registrationService = mock(RegistrationService.class);
        User user = exampleGetUsersMockDate().get(0);
        User badUser = exampleGetUsersMockDate().get(1);
        given(registrationService.findUserByEmailAndPassword(user.getEmail(), user.getPassword())).willReturn(user);
        RegistrationController registrationController = new RegistrationController(registrationService, null);
        //When
        User user1 = registrationController.loginUser(user);
        //Then
        assertEquals(user, user1);
        Assertions.assertThrows(RuntimeException.class, () -> registrationController.loginUser(badUser));
    }

    private List<User> exampleGetUsersMockDate() {
        List<User> users = new ArrayList<>();
        users.add(new User(
                1L,
                "User_1",
                "Haslo_1",
                "Email_1",
                Roles.USER,
                "Code_1"
        ));
        users.add(new User(
                2L,
                "User_2",
                "Haslo_2",
                "Email_2",
                Roles.USER,
                "Code_2"

        ));
        return users;
    }
}