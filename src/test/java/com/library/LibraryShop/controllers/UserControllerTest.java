package com.library.LibraryShop.controllers;

import com.library.LibraryShop.models.Roles;
import com.library.LibraryShop.models.User;
import com.library.LibraryShop.service.UserService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class UserControllerTest {

    @Test
    void getAllUsers() {
        //Given
        UserService userService = mock(UserService.class);
        given(userService.getUsers()).willReturn(exampleGetUsersMockDate());
        UserController userController = new UserController(userService);
        //When
        List<User> users = userController.getAllUsers();
        //Then
        assertEquals(users, exampleGetUsersMockDate());
    }

    @Test
    void getUserById() {
        //Given
        UserService userService = mock(UserService.class);
        User tempUser = exampleGetUserByIdMockDate(1L);
        given(userService.findUserById(tempUser.getId())).willReturn(tempUser);
        UserController userController = new UserController(userService);
        //When
        User user = userController.getUserById(tempUser.getId());
        //Then
        assertEquals(user, tempUser);
    }

    @Test
    void addUser() {
        //Given
        UserService userService = mock(UserService.class);
        User tempUser = exampleGetUsersMockDate().get(0);
        given(userService.addUser(tempUser)).willReturn(tempUser);
        UserController userController = new UserController(userService);
        //When
        User user = userController.addUser(tempUser);
        //Then
        assertEquals(user, tempUser);
    }

    @Test
    void updateUser() {
        //Given
        UserService userService = mock(UserService.class);
        User tempUser = exampleGetUsersMockDate().get(0);
        given(userService.updateUser(tempUser)).willReturn(tempUser);
        UserController userController = new UserController(userService);
        //When
        User user = userController.updateUser(tempUser);
        //Then
        assertEquals(user, tempUser);
    }

    @Test
    void deleteUser() {
        //Given
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
        long userId = 1L;
        //When
        userController.deleteUser(userId);
        //Then
        verify(userService, times(1)).deleteUser(userId);
    }

    private List<User> exampleGetUsersMockDate() {
        List<User> users = new ArrayList<>();
        users.add(new User(
                1L,
                "User_1",
                "Email_1",
                "Code_1",
                Roles.USER,
                "Haslo_1"
        ));
        users.add(new User(
                2L,
                "User_2",
                "Email_2",
                "Code_2",
                Roles.USER,
                "Haslo_2"

        ));
        return users;
    }

    private User exampleGetUserByIdMockDate(long userId) {
        var ref = new User() {
            User user = new User();
        };
        List<User> users = exampleGetUsersMockDate();
        users.forEach(u -> {
            if (u.getId() == userId)
            {
                ref.user = u;
            }
        });
        return ref.user;
    }
}